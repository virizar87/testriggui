import numpy as np
import time
import socket
import xlrd


# -----------------------------------------------------------------
# ----- CLASS: WriteDAQData (Writes Data in Different Formats -----
# -----------------------------------------------------------------
class WriteDAQData:

    # _____INIT METHOD: Initialize Class, register Variables____
    def __init__(self, data, outType, fileName, workDir, fileLength, channelDesc):

        self.data = data                                                # Data to be printed
        self.outType = outType                                          # Type of Format Output
        self.fileName = fileName                                        # Name of the File to be printed
        self.workDir = workDir                                          # Working Directory
        self.selFileLoc = self.workDir + '/' + self.fileName + '.sel'   # File Location
        self.printCount = self.data.shape[0]                            # Number of rows to be printed
        self.numChannels = self.data.shape[1]                           # Number of channels
        # TODO: HANDLE WHEN A PLC SENDS 0 DATA!
        self.fileLength = fileLength                                    # Size of the file in seconds
        self.channelDesc = channelDesc                                  # Description of the channels
        self.scaleFactors = np.ones(self.numChannels)                   # Scale factors for binary data
        self.binConv = float(pow(2, 15)-1)                              #  Binary Converssion (4 bytes, 2^16)

    # _____WRITE DATA FILE METHOD: Write the corresponding data file with format____
    def write_data(self):

        # 0 == HAWC2 Ascci
        # 1 == HAWC2 binary
        # TODO: Add other format types
        if self.outType == 0:
            self.hawc2_ascii()
        elif self.outType == 1:
            self.hawc2_binary()
        elif self.outType == 2:
            print 'Printing CSV'
        elif self.outType == 3:
            print 'Another TYPE????'

    # _____WRITE SEL FILE METHOD: Write the channel description file____
    def write_sel_file(self, scaleFactors):

        # Open .sel File on Location
        selFile = open(self.selFileLoc, 'a')

        # Check the type of output and insert it in the file
        if self.outType == 0:
            fileFormat = 'ASCII'
        elif self.outType == 1:
            fileFormat = 'BINARY'
        else:
            fileFormat = 'UNKNOWN'

        # Create .sel File
        # Print Header with information of the run
        sep = '_' * 130
        selFile.write(sep + '\n')
        selFile.write('  Data Description File Based for FSE/DAQ\n')
        selFile.write('  Version : 1.0\n')
        selFile.write('  Date: %s\n' % time.asctime(time.localtime(time.time())))
        selFile.write(sep + '\n')
        selFile.write('  Results File: %s\n' % (self.fileName + '.dat'))
        selFile.write(sep + '\n')
        # Print the characteristics of the channels
        selFile.write(' %10s %10s %10s %10s\n' % ('Scans', 'Channels', 'Time[s]', 'Format'))
        selFile.write(' %10i %10i %10.4f %10s\n\n' % (self.printCount, self.numChannels, self.fileLength, fileFormat))
        # Print the description of the variables
        selFile.write('  Channel   Variable Description\n\n')

        for i in range(self.numChannels):
            if i < len(self.channelDesc):
                # selFile.write('    %i  %s  %s  %s\n' % (i, 'var' + str(i), 'unit' + str(i), 'desc' + str(i)))
                selFile.write('    %i      %-32s%-13s%s\n' % (i, self.channelDesc[i][1], self.channelDesc[i][2], self.channelDesc[i][3]))
            else:
                selFile.write('    %i    %4s%20s%4s\n' % (i, 'Unknown var' + str(i), 'Unknown unit' + str(i), 'Unknown desc' + str(i)))

        selFile.write(sep + '\n')
        # Print the conversion numbers for each of the channels
        if self.outType == 1:
            selFile.write('Scale factors:\n')
            for i in range(self.numChannels):
                selFile.write(('  {:.6E}' + '\n').format(scaleFactors[i]))

        # Close the file
        selFile.close()

    # _____HAWC2 ASCII METHOD: Write the file in HAWC2 Ascii format____
    def hawc2_ascii(self):

        # Create the datafile and Open
        datafileLoc = self.workDir + '/' + self.fileName + '.dat'
        dataFile = open(datafileLoc, 'a')

        # For each channel print the data
        for i in range(self.printCount):
            daqValues = self.data[i,:]
            dataFile.write(('  {:.6E}' * self.numChannels + '\n').format(*daqValues))

        # Close the data file
        dataFile.close()

        # Create sel file
        self.write_sel_file(0)

    # _____HAWC2 BINARY METHOD: Write the file in HAWC2 BINARY format____
    def hawc2_binary(self):

        # Create the datafile and Open
        datafileLoc = self.workDir + '/' + self.fileName + '.dat'
        dataFile = open(datafileLoc, 'ab')

        # Convert the data into binary
        binResults = self.dat_to_bin()

        # Write the binary data
        dataFile.write(binResults['binData'])

        # Close the data file
        dataFile.close()

        # Create sel file
        self.write_sel_file(binResults['scaleFacts'])

    # _____DAT TO BIN METHOD: COnvert the data into binary____
    def dat_to_bin(self):

        # Create the scale factor vectors
        scaleFactors = np.zeros(self.numChannels)
        scaleFactorsInv = np.zeros(self.numChannels)

        # CAlculate the minimum and maximum values of each channel
        minData = self.data.min(0)
        maxData = self.data.max(0)

        # For every channel calculate the scaling factors
        for i in range(self.numChannels):

            # Calculate scale factor (Reference to HAWC2 Output)
            scaleFactors[i] = np.maximum(np.abs(minData[i]), np.abs(maxData[i]))/self.binConv

            # Try to calculate the inverted factors, if not division by 0, defaulted to 0
            with np.errstate(divide='raise'):
                try:
                    scaleFactorsInv[i] = 1/scaleFactors[i]
                except FloatingPointError:
                    scaleFactorsInv[i] = 0.0
                    pass

        # Create the binary data
        binData = np.multiply(self.data, scaleFactorsInv).astype('uint16').tobytes('F')

        # Return the data and the scaling factors
        return {'binData': binData, 'scaleFacts': scaleFactors}


# --------------------------------------------------------------------
# ----- CLASS: NamedSocket (Socket Class with Added Properties)  -----
# --------------------------------------------------------------------
class NamedSocket(socket.socket):

    # _____INIT METHOD: Initialize Class, register Variables____
    def __init__(self, sockName, ip, port, varDesc, dcdStr):

        # Initialize the socket as UDP
        socket.socket.__init__(self, socket.AF_INET, socket.SOCK_DGRAM)
        self.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        self.setblocking(0)                 # Set blovking to 0 (no timeout)
        self.sockName = sockName            # Socket Name (PLC Name)
        self.varDesc = varDesc              # Description of the transfered variables
        self.ipAddr = ip                    # Ip address of the socket
        self.port = port                    # Port of the Socket
        self.decodeStr = dcdStr             # Decode string for pack/unpack data variables
        # TODO: Implement the check connection in the read files
        self.performConnCheck = ''          # Perform Connection Check (UNUSED)


# ---------------------------------------------------------------
# ----- CLASS: PLCSockConfig (PLC Description File Reader)  -----
# ---------------------------------------------------------------
class PLCSockConfig:

    # _____INIT METHOD: Initialize Class, register Variables____
    def __init__(self, plcFile):
        self.sockList = []              # Socket List vector
        self.plcFile = plcFile          # PLC description file name

    # _____GET_PLC_CONFIG METHOD: Loads PLC Configuration file, reads and extracts data____
    def get_plc_config(self):

        # Open Workbook and Read Cells
        # TODO: Add exception handling for type of file and size of cells, etc
        wb = xlrd.open_workbook(self.plcFile)
        sheetNames = wb.sheet_names()

        # Loop through the number of sheets (number of PLC's)
        for i, name in enumerate(sheetNames):

            # If there is the PLC word and not ignore (Adds PLC or ignores PLC's)
            if 'PLC' in name and 'ignore' not in name:

                # Load the worksheet's contents
                workSheet = wb.sheet_by_index(i)

                # Extract the data of the second row (name, ip address, port, num channels)
                plcConfig = [val.value for val in workSheet.row(1)[:-1]]
                plcConfig[2] = int(plcConfig[2])
                plcConfig[3] = int(plcConfig[3])

                # Initialize the description matrix and decode string
                descMat = []
                decodeStr = ''

                # Loop in every row of data
                for i in range(5, 5 + plcConfig[3]):

                    # Extract the name, unit description of the variable
                    data = [str(val.value) for val in workSheet.row(i)[:-1]]
                    descMat.append(data)

                    # Append the character of the decode string
                    decodeStr = decodeStr + workSheet.row(i)[4].value

                # Append the data to an assigned Named Socket
                self.sockList.append(NamedSocket(plcConfig[0], plcConfig[1], plcConfig[2], descMat, decodeStr))

        return self.sockList
