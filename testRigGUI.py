import Queue
import Tkinter as tk
import os
import socket
import threading
import time
import tkFileDialog as tkFD
import tkMessageBox as tkMB
import ttk
from idlelib.WidgetRedirector import WidgetRedirector
import fnmatch
import numpy as np
from daqTools import WriteDAQData, PLCSockConfig
import logging
import sys
import select
import struct


# String to number function
def num(s):
    try:
        return int(s)
    except:
        try:
            return float(s)
        except:
            return float('nan')


# --------------------------------------------------------------
# ----- CLASS: ReadOnlyText (Modified Tkinter Text Widget) -----
# --------------------------------------------------------------
class ReadOnlyText(tk.Text):
    # _____INITIALIZATION METHOD: Inherited from Text widget____
    def __init__(self, *args, **kwargs):
        # Initialize Text Widget
        tk.Text.__init__(self, *args, **kwargs)

        # Setup Redirector for new insert/delete functions
        self.redirector = WidgetRedirector(self)
        self.insert = self.redirector.register("insert", lambda *args, **kw: "break")
        self.delete = self.redirector.register("delete", lambda *args, **kw: "break")


# ---------------------------------------------------
# ----- CLASS: MainWindow (Main GUI of the App) -----
# ---------------------------------------------------
class MainWindow:
    # _____INITIALIZATION METHOD: Setup Gui and Variables____
    def __init__(self, parent, queue, *args, **kwargs):

        #########################
        ##### GUI: Variables ####
        #########################

        # LOGGING VARIABLES
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)
        handler = logging.FileHandler('debug.log')
        handler.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)
        sys.excepthook = self.excp_handler

        # LOG
        self.logger.info('Program Started')

        # VAriables of the multithreading
        self.queue = queue  # Shared Queue for printing data to console

        # Checks for the GUI
        self.varCheckMultFiles = tk.IntVar()  # Value of the check for multiple files
        self.varEnableTrigger = tk.IntVar()  # Value of the check for enabling triggering with PLC count
        self.varMultiFileLabel = tk.IntVar()  # Selection of the labeling type for multiple files
        self.varFileNumSet = 0                  # Value of the type of numbering for the files
        self.varOutType = 0                     # Value of the type of output selected
        self.triggerSocket = ''

        # Read the PLC config file
        # TODO: Exception handling of the plcCOnfig data
        self.plcCfg = PLCSockConfig('TR_plcConfig.xlsx')
        self.sockList = self.plcCfg.get_plc_config()

        # States of the program
        self.varInitialized = False  # Initialized State
        self.varRunning = False  # Running State
        self.varPaused = False  # Paused State
        self.varStopDAQ = False     # Stop DAQ
        self.varCheckUDPCount = 0  # Successful Connections counter
        self.connStates = [0 for _ in range(len(self.sockList))] # Connection States of the Sockets
        self.vecIsBound = [0 for _ in range(len(self.sockList))] # States of the bound sockets
        # TODO : Add the enable/disable flag in the excel file of the plcs
        self.vecPerformConnCheck = [0 for i in range(len(self.sockList))] # Enable/ Disable (1/0) Connection checking of PLCs

        # Directory Settings
        self.varWorkDir = ''  # Working Directory
        self.varProjName = ''  # File Names
        self.varFileLength = 0  # length of the daq file in seconds
        self.varProjectDir = ''  # Project directory (Combination of WorkDir+ProjDir)

        ############################
        #### GUI: Layout Frames ####
        ############################

        # Make Main frames
        # Setup Frame
        self.setupFrame = tk.Frame(parent)
        self.setupFrame.pack(padx=5, pady=5, side='left', fill='y')
        # Console Frame
        self.consoleFrame = tk.LabelFrame(parent, text='Console Out')
        self.consoleFrame.pack(padx=5, pady=10, side='left', fill='y')

        # Set setup options on left frame
        # directory frame Options
        self.direcFrameOps = tk.LabelFrame(self.setupFrame, text='Directory Options')
        self.direcFrameOps.pack(padx=5, pady=5, ipadx=5, ipady=5)  # grid(row=0,column=0, padx=5, pady=5)
        # setup Frame Options
        self.setupFrameOps = tk.LabelFrame(self.setupFrame, text='Data File Options')
        self.setupFrameOps.pack(fill='x', padx=5, pady=5)  # grid(row=2,column=0, sticky=tk.W, padx=5, pady=5)
        # run and initialization frame
        self.runFrame = tk.LabelFrame(self.setupFrame, text='Run DAQ Control')
        self.runFrame.pack(fill='x', padx=5, pady=5)  # grid(row=2,column=0, sticky=tk.W, padx=5, pady=5)

        #######################
        #### GUI: MENU BAR ####
        #######################

        # TODO: Include any options in the menu Bar (File, Edit, COnsole)
        self.menuBar = tk.Menu()

        # File Menus
        self.fileMenu = tk.Menu(self.menuBar, tearoff=0)
        self.menuBar.add_cascade(label='File', menu=self.fileMenu)
        self.fileMenu.add_command(label='Option 1', accelerator="Ctrl + N", compound='left')
        self.fileMenu.add_command(label='Option 2', accelerator="Ctrl + L", compound='left')

        # Edit Menu
        self.editMenu = tk.Menu(self.menuBar, tearoff=0)
        self.menuBar.add_cascade(label='Edit', menu=self.editMenu)

        # Configure the menu bar
        parent.config(menu=self.menuBar)

        ##########################################################################
        ####### GUI: DirecFrameOps - Options for Directory and File names ########
        ##########################################################################

        # Project name
        tk.Label(self.direcFrameOps, text='Project Name').grid(row=0, column=0, sticky='w', padx=5)
        self.projNameEntry = tk.Entry(self.direcFrameOps, width=60)
        self.projNameEntry.grid(row=1, column=0, sticky='w', padx=5, columnspan=3)
        self.projNameEntry.insert(tk.END, 'defaultTest')  # for easier debugging

        # Project Location Buttons
        tk.Label(self.direcFrameOps, text='Select Project Location...').grid(row=2, column=0, sticky='w', padx=5)
        self.projLocEntry = tk.Entry(self.direcFrameOps, width=60)
        self.projLocEntry.grid(row=3, column=0, sticky='w', padx=5, columnspan=2)
        self.projLocEntry.insert(tk.END, 'C:/Users/Victor/Desktop/FSE/testRigTests')  # for easier debugging
        tk.Button(self.direcFrameOps, text='...', command=self.get_dir_name, font=('helvetica', 7)).grid(row=3,
                                                                                                         column=2,
                                                                                                         columnspan=2,
                                                                                                         sticky='sw',
                                                                                                         padx=5, pady=0)

        ##########################################################################
        ####### GUI: setupFrameOps- Options for the printing of data fle 3########
        ##########################################################################

        # length of the file in seconds
        tk.Label(self.setupFrameOps, text='Length of the data file [s]').grid(row=0, column=0, sticky='w', padx=5)
        self.fileLenEntry = tk.Entry(self.setupFrameOps, width=10)
        self.fileLenEntry.insert(tk.END, 60)
        self.fileLenEntry.grid(row=0, column=1, sticky='w', padx=5)

        # Write several Files
        self.checkMultFiles = tk.Checkbutton(self.setupFrameOps, text="Write data in several files",
                                             variable=self.varCheckMultFiles,
                                             onvalue=1, offvalue=0)
        self.checkMultFiles.grid(row=5, column=0, sticky='w', padx=5)
        # Numbering style of the files
        self.label_fileNumberSet = tk.Label(self.setupFrameOps, text='Choose Numbering Style')
        self.fileNumberSet = ttk.Combobox(self.setupFrameOps,
                                          values=['Timestamp (YYYYMMDD_hhmm)', 'Numbering (_1, _2 ...)'], width=30,
                                          state='readonly')
        self.fileNumberSet.current(0)  # Default to timestamp
        self.label_fileNumberSet.grid(row=2, column=0, sticky='w', padx=5, pady=5)
        self.fileNumberSet.grid(row=2, column=1, sticky='w', padx=5, pady=5, columnspan=2)
        # Type of data file to be written
        self.label_outTypeSet = tk.Label(self.setupFrameOps, text='Choose File Format')
        self.outTypeSet = ttk.Combobox(self.setupFrameOps,
                                          values=['ASCII Subset', 'BINARY Subset'], width=30,
                                          state='readonly')
        self.outTypeSet.current(1)  # Default to bynary
        self.label_outTypeSet.grid(row=3, column=0, sticky='w', padx=5, pady=5)
        self.outTypeSet.grid(row=3, column=1, sticky='w', padx=5, pady=5, columnspan=2)

        # Enable triggering with PLC count
        self.enableTriggering = tk.Checkbutton(self.setupFrameOps, text="Enable Triggering with PLC",
                                               variable=self.varEnableTrigger, onvalue=1, offvalue=0)
        self.enableTriggering.grid(row=4, column=0, sticky='w', padx=5)
        self.triggerList = ttk.Combobox(self.setupFrameOps,
                                        values=[sock.sockName for sock in self.sockList], width=30,
                                        state='readonly')
        self.triggerList.grid(row=4, column=1, sticky='w', padx=5)

        ################################################
        ####### GUI: runFrame - running buttons ########
        ################################################

        self.runButton = tk.Button(self.runFrame, text='Run DAQ', command=self.run_daq, state='disabled')
        self.runButton.pack(side=tk.RIGHT, padx=2, pady=5, expand='yes')
        self.pauseButton = tk.Button(self.runFrame, text='Pause', command=self.pause_daq, state='disabled')
        self.pauseButton.pack(side=tk.RIGHT, padx=2, pady=5, expand='yes')
        self.stopButton = tk.Button(self.runFrame, text='Stop', command=self.stop_daq, state='disabled')
        self.stopButton.pack(side=tk.RIGHT, padx=2, pady=5, expand='yes')
        self.initButton = tk.Button(self.runFrame, text='Check Connection', command=self.init_daq)
        self.initButton.pack(side=tk.RIGHT, padx=2, pady=5, expand='yes')

        ###################################
        ####### GUI: consoleFrame  ########
        ###################################

        # Scrollbar for the output console
        self.scrollbar = tk.Scrollbar(self.consoleFrame)
        self.scrollbar.pack(side='right', fill='y')

        # Console creation
        self.consoleOut = ReadOnlyText(self.consoleFrame, height=20, width=80, bd=0,
                                       background=self.consoleFrame.cget('background'))
        self.consoleOut.pack(padx=1, pady=1)

        # Attach the scrollbar motion to the textbox scrolling
        self.consoleOut.config(yscrollcommand=self.scrollbar.set)
        self.scrollbar.config(command=self.consoleOut.yview)

        # Print the status as the software starts
        self.consoleOut.insert(tk.INSERT, 'FSE-GW DAQ Software for Test Rig\n')
        self.consoleOut.insert(tk.INSERT, 'Program Started: %s\n' % time.asctime(time.localtime(time.time())))
        self.consoleOut.insert(tk.INSERT, 'Listening to the following IPs and Ports:\n')

        for sock in self.sockList:
            self.consoleOut.insert(tk.INSERT, ' - IP: %s, Port:%i for the %s\n' % (
                sock.ipAddr, sock.port, sock.sockName))

        self.consoleOut.insert(tk.INSERT, '\nWaiting for initialisation and connection...\n')

    # _____EXCP HANDLER METHOD: Tunnels exception handling to the debug file____
    def excp_handler(self, type, value, tb):
        self.logger.exception("UNCAUGHT EXCEPTION!!!: {0}".format(str(value)))

    # _____GET DIRECTORY METHOD: Open Prompt and Select Project Directory____
    def get_dir_name(self):

        # Retrieve the project location from the project dir prompt
        projDir = tkFD.askdirectory()

        # Insert the string of the file location in the respective entry
        self.projLocEntry.delete(0, tk.END)
        self.projLocEntry.insert(0, projDir)

        # LOG
        self.logger.info('Getting Directory: %s' % projDir)

    # _____RUN DAQ METHOD: Run the data Aqcuisition____
    def run_daq(self):
        try:

            # Assert that all the options have been selected
            assert (self.projLocEntry.get() != '')
            assert (self.projNameEntry.get() != '')
            assert (int(self.fileLenEntry.get()) > 0)

            # Assign values of the directory settings to respective variables (Enables Changes after the Initialization)
            self.varWorkDir = self.projLocEntry.get()
            self.varFileLength = num(self.fileLenEntry.get())
            self.varProjName = self.projNameEntry.get()
            self.varProjectDir = self.varWorkDir + '/' + self.varProjName
            self.varFileNumSet = self.fileNumberSet.current()
            self.varOutType = self.outTypeSet.current()
            self.triggerSocket = self.triggerList.get()

            # Check if the filename exist in the folder
            for file in os.listdir(self.varWorkDir):
                if fnmatch.fnmatch(file, self.varProjName + '*'):
                    raise OSError

            # Set the running state to true
            self.varRunning = True

            # Alert the user that the DAQ is starting
            self.consoleOut.insert('end',
                                   'Starting Data Acquisition: %s\n\n' % time.asctime(time.localtime(time.time())))

            # LOG
            self.logger.info('Running DAQ (%s, %0.2fs, %s, %i, %i)' % (self.varWorkDir, self.varFileLength,
                                                                      self.varProjName, self.varFileNumSet,
                                                                      self.varOutType))

        # Catch exceptions
        except AssertionError:
            # If there is any field missing for the directory setup
            tkMB.showwarning('Warning', 'Please select all proper fields (Directories, File Name and File Length)')

            # LOG
            self.logger.error('All Fields were not selected properly.')

        except OSError:
            # If there is a project with the same name
            tkMB.showwarning('Warning',
                             'There is an existing project with the specified name in the folder. Please change the project name.')

            # LOG
            self.logger.error('Already an existing project with the same name in the directory')

    # _____DAQ INITIALIZATION METHOD: Check UDP Connections and Rest of the Setup____
    def init_daq(self):

        # LOG
        self.logger.info('Initializing DAQ...')

        # Check Port Connections
        self.check_udp_conn()

    # _____STOP DAQ METHOD: Stop the DAQ run____
    def stop_daq(self):

        # LOG
        #self.logger.info('Stopping DAQ...')
        
        # Check the stop variable
        if self.varRunning:
            
            self.varStopDAQ = True
            print 'stopping', self.varStopDAQ
        else:
            self.varStopDAQ = False

    # _____PAUSE DAQ METHOD: Pause/Resume the DAQ run____
    def pause_daq(self):

        # Toggle the pause variable
        self.varPaused = not self.varPaused

        # Change Button label to resume/pause
        if self.varPaused:
            self.pauseButton.config(text='Resume')
            # LOG
            self.logger.info('DAQ Paused...')
        else:
            self.pauseButton.config(text='Pause')
            # LOG
            self.logger.info('DAQ Resumed...')

    # _____CHECK UDP CONNECTIONS METHOD: CHECK IP/PORTS and try to bind sockets____
    def check_udp_conn(self):
        # TODO: Do the udp check in another thread to avoid frozen gui??????
        # Print to console
        self.consoleOut.delete('1.0 + 9l', 'end')
        self.consoleOut.insert('end', '\nInitializing UDP...\n')

        for i, sock in enumerate(self.sockList):
            try:
                # Check the state of the socket
                # If it is not connected
                if self.connStates[i] == 0:
                    # try to bind the socket
                    if self.vecIsBound[i] == 0:
                        sock.bind((sock.ipAddr, sock.port))
                        self.vecIsBound[i] = 1

                    # Perform the connection check (Receive one packet or timeout)
                    if self.vecPerformConnCheck[i] == 1 and self.vecIsBound[i] == 1:
                        sock.settimeout(1)
                        data, addr = sock.recvfrom(1024)

                    # Alert Succesful Connection
                    self.consoleOut.insert('end', ' - [OK!] %s succesfully connected\n' % sock.sockName)

                    # LOG
                    self.logger.info(' %s succesfully connected' % sock.sockName)

                    # Increase the socket counter
                    self.varCheckUDPCount += 1

                    # Change the state of the socket
                    self.connStates[i] = 1

                # If the socket is already bound
                elif self.connStates[i] == 1:
                    self.consoleOut.insert('end', ' - [OK!] %s succesfully connected\n' % sock.sockName)
                    # LOG
                    self.logger.info(' %s succesfully connected' % sock.sockName)

            except socket.timeout:
                # If the socket Times Out alert the user
                self.consoleOut.insert('end', ' - [ERROR!] %s timed out.\n' % sock.sockName)
                # LOG
                self.logger.error(' %s timed out.' % sock.sockName)

            except socket.error, v:
                # If the socket could not be bound, alert the user
                print v
                self.consoleOut.insert('end',
                                       ' - [ERROR!] %s not connected.\n\t%s, %s\n' % (sock.sockName, v[0], v[1]))
                # LOG
                self.logger.error(' %s timed out.' % sock.sockName)

        # check if all the connections are ok, if not close and retry
        if sum(self.connStates) == len(self.sockList):
            self.consoleOut.insert('end',
                                   '\nPress Run DAQ Button to start acquisition...\n\n')
            self.varInitialized = True
            # LOG
            self.logger.info(' All the PLCS Connected and correctly initialized ')
        else:
            print 'closing connections'
            self.consoleOut.insert('end',
                                   '\nSome UDP connections could not be established.\nPlease check IP/Ports and Physical Connections\n')
            self.varInitialized = False
            # LOG
            self.logger.info('Some UDP connections could not be established. Please check IP/Ports and Physical Connections')

    # _____DAQ RUN INCOMING METHOD: Check Periodically and print to console when DAQ is running____
    def daq_run_incoming(self):

        # While there are elements in the queue
        while self.queue.qsize():
            try:
                # get message from queue
                msg = self.queue.get(0)

                # Message from the DAQ Loop functioning
                if msg[3] == 0:
                    # Print progress message to console
                    self.consoleOut.delete('end - 2l', 'end')
                    self.consoleOut.insert('end', '\n    - Writing File %i: %s - %i%s\n' % (msg[0], msg[1], msg[2], '%'))
                # Error from the DAQ loop
                elif msg[3] == 1:
                    self.consoleOut.insert('end', '%s' % (msg[0]))
                elif msg[3] == 2:
                    self.consoleOut.delete('end - 1l', 'end')
                    self.consoleOut.insert('end', '%s' % (msg[0]))

            except Queue.Empty:
                # just on general principles, although we don't
                # expect this branch to be taken in this case
                pass


# -----------------------------------------------------------------
# ----- CLASS: ThreadedClient (Handles MultiThreading in GUI) -----
# -----------------------------------------------------------------
class ThreadedClient:
    # _____INITIALIZATION METHOD: Calls GUI (MainWindow)and Sets Threads____
    def __init__(self, master):

        # Reference to the master widget
        self.master = master

        self.finishedDAQ = False
        self.writing = False
        self.triggerWait = True

        # Create the queues
        self.queue = Queue.Queue()              # Status printing in console for the GUI
        self.writeQueue = Queue.Queue()         # Write queue for making data files

        # Set up the GUI part
        self.gui = MainWindow(master, self.queue)

        # States of the DAQ
        self.running = False
        self.threadDAQActive = False
        self.stopDAQ = False
        self.threadWriteActive = False
        self.timex = 0  # Experimental Timer (TO BE CHANGED)

        # Handle the closing of the program
        master.protocol('WM_DELETE_WINDOW', self.close_program)

        # Start the periodic call in the GUI
        self.periodic_call()

    # _____PERIODIC CALL METHOD: Checks GUI every 200ms for Changes____
    def periodic_call(self):

        # Run the Process Incoming Methods of the GUI
        self.gui.daq_run_incoming()

        # Store the running variable internally
        self.running = self.gui.varRunning
        self.stopDAQ = self.gui.varStopDAQ

        # Enable the control buttons (pause and stop)
        if not self.running:
            self.gui.pauseButton.config(state='disabled')
            self.gui.stopButton.config(state='disabled')
            self.gui.runButton.config(state='normal')
        else:
            self.gui.pauseButton.config(state='normal')
            self.gui.stopButton.config(state='normal')
            self.gui.runButton.config(state='disabled')

        # Enable/disable the setup frames
        if self.gui.varRunning:
            # Disable the setup frames while running the DAQ
            for child in self.gui.direcFrameOps.winfo_children():
                child.configure(state='disable')
            for child in self.gui.setupFrameOps.winfo_children():
                child.configure(state='disable')
        else:
            # Enable them when not Running
            for child in self.gui.direcFrameOps.winfo_children():
                child.configure(state='normal')
            for i, child in enumerate(self.gui.setupFrameOps.winfo_children()):
                if i == 1:
                    child.configure(state='normal')
                else:
                    try:
                        child.configure(state='readonly')
                    except tk.TclError:
                        child.configure(state='normal')

            if self.gui.varEnableTrigger.get() == 1:
                self.gui.triggerList.config(state='readonly')
            else:
                self.gui.triggerList.config(state='disable')

        # Check if there has been a timeout during the DAQ loop, re-enable Initialization Button
        # Once everything is checked activate all the DAQ options
        if self.gui.varInitialized:
            # Enable the buttons and alert that the DAQ is ready to use
            self.gui.initButton.config(text='Initialized OK!', state='disabled')
        else:
            self.gui.initButton.config(text='Check Connection', state='normal')
            self.gui.runButton.config(state='disabled')

        # Check that DAQ is not running and the thread is inactive
        if self.running and not self.threadDAQActive:

            # Enable the trigger wait if the option is ticked
            if self.gui.varEnableTrigger.get() == 1:
                self.triggerWait = True

            # Writing and DAq threads
            daqThread = threading.Thread(target=self.thread_daq_run)
            self.threadDAQActive = True
            daqThread.start()
            writeThread = threading.Thread(target=self.thread_daq_writedata)
            self.threadWriteActive = True
            writeThread.start()

        # Scroll the console to the end of the text widget
        if self.running and not self.gui.varPaused:

            try:
                self.gui.consoleOut.see('end')
            # TODO : check the name of the error being raised when scrolling the end
            except:
                pass

        else:

            # Disable the stop button when DAQ is paused
            self.gui.stopButton.config(state='disabled')

        # Call the periodic call method recursively every 200 ms
        self.master.after(200, self.periodic_call)

    # ____THREAD DAQ WRITEDATA METHOD: Writes data files in respective file format____
    def thread_daq_writedata(self):

        # Wait for the trigger to happen when the Enable Trigger optio is enabled
        if self.gui.varEnableTrigger.get() == 1:
            self.gui.logger.info(' Waiting for Trigger...')
            while self.triggerWait:
                time.sleep(0.01)

        # LOG
        self.gui.logger.info(' Running DAQ writing thread...')

        # Initialize local variables
        fileNum = 0
        printCount = 0
        fileNumSet = self.gui.varFileNumSet
        multiFiles = self.gui.varCheckMultFiles.get()
        outType = self.gui.varOutType
        workDir = self.gui.varWorkDir

        while self.gui.varRunning:

            # Create The Data Filename
            if fileNumSet == 0:
                date = time.localtime(time.time())
                fileName = '%sNo%04d_%04d%02d%02d_%02d%02d' % (
                    self.gui.varProjName, fileNum, date[0], date[1], date[2], date[3], date[4])
            elif fileNumSet == 1:
                fileName = '%sNo_%04d' % (self.gui.varProjName, fileNum)

            # Initialize printing time
            printTime0 = 0
            printTime = 0

            # Vector for holding the data to be printed (one vector for each plc)
            printData = {sock.getsockname(): [] for sock in self.gui.sockList}

            # Start Writing
            self.writing = True

            while self.writing:

                try:

                    # Reset the pause counter
                    pauseCount = 0

                    # Pause the writing procedure
                    while self.gui.varPaused:
                        time.sleep(0.1)
                        if pauseCount == 0:
                            self.queue.put(
                                ['    - DAQ Paused by User: %s\n\n' % time.asctime(time.localtime(time.time())), '', 0, 1])
                            self.gui.logger.info(' DAQ Paused...')
                        pauseCount += 1
                    # get message from queue
                    msg = self.writeQueue.get()

                    # Check if the daq has been stopped by the daq thread, if not, print progress
                    if msg == 'daqend':
                        self.gui.logger.info('Stopping Daq')
                        self.writing = False
                        self.running = False
                        self.gui.varRunning = False
                        self.gui.varStopDAQ = True
                        self.queue.put(
                            ['    - DAQ Stopped by User : %s\n\n' % time.asctime(time.localtime(time.time())), '', 0, 1])
                        break
                    # Check if the stop comes from a timeout in the sockets
                    elif msg == 'daqendtimeout':
                        self.writing = False
                        self.running = False
                        self.gui.varRunning = False
                        self.gui.varStopDAQ = True
                        self.queue.put(
                            ['    - DAQ Finished Unsuccessfully : %s\n\n' % time.asctime(time.localtime(time.time())), '', 0,
                             1])
                        break
                    else:

                        # loop through socket list and distribute the data
                        for sock in self.gui.sockList:
                            if msg[1] == sock.getsockname():
                                row = []
                                msg[2].insert(0, msg[0])
                                row.extend(msg[2])
                                printData[sock.getsockname()].append(row)
                                pass

                        # Calculate the initial time
                        if printCount == 0:
                            printTime0 = msg[0]

                        # Calculate Time (to check when to stop)
                        printTime = msg[0] - printTime0

                        # Take care of the negative print Times caused by trailing values of previous runs in queue
                        if printTime < 0.0:
                            printTime = 0.0
                            printTime0 = msg[0]

                        # Calculate percentage of the file being written
                        percent = min(np.ceil(printTime / float(self.gui.varFileLength) * 100), 100.0)

                        # Every 10% of the file, send the percent and filename to be printed in GUI
                        if percent % 1 == 0:
                            self.queue.put([fileNum, fileName, percent, 0])

                    # If the time is larger than the specified file time length
                    if printTime >= self.gui.varFileLength:
                        self.writing = False

                    # Increase print count
                    printCount += 1

                except Queue.Empty:
                    # just on general principles, although we don't
                    # expect this branch to be taken in this case
                    print 'queue empty '
                    pass

            # Create a data printing object
            for sock in self.gui.sockList:
                # Construct PLC filename
                plcFileName = fileName + '_%s' % (sock.sockName)

                # Default time label of the PLC timestamp
                timeLabel = '0,DAQTime,s,Timestamp of DAQ'.split(',')

                if sock.varDesc[0] != timeLabel:
                    sock.varDesc.insert(0, timeLabel)

                # If there is data in the matrices, write data to file
                if printData[sock.getsockname()]:
                    daqWriter = WriteDAQData(np.array(printData[sock.getsockname()]), outType, plcFileName, workDir, printTime, sock.varDesc)
                    daqWriter.write_data()

            # Break the writing of the files if no multiple files will be used
            if multiFiles == 0:
                break

            # Increase the file counter and reset the print count
            fileNum += 1
            printCount = 0

        # Check if the run was stopped
        if not self.gui.varStopDAQ:
            # Alert the user that the Daq finished succsefully
            self.queue.put(
                ['    - DAQ Finished Successfully : %s\n\n' % time.asctime(time.localtime(time.time())), '', 0, 1])

        # Set the states back to normal
        self.gui.varRunning = False
        self.running = False
        self.threadDAQActive = False
        self.gui.varStopDAQ = False
        self.gui.varRunning = False

        # clean the queue
        self.writeQueue.queue.clear()

        # LOG
        self.gui.logger.info(' Writing Thread succesfully executed!')

    # _____THREAD DAQ RUN METHOD: Runs the DAQ sends data to other thread to write____
    def thread_daq_run(self):

        # Set the thread to active
        self.threadDAQActive = True

        # Empty the buffers of the udp receiving sockets
        # LOG
        self.gui.logger.info(' Emptying Sockets...')
        self.queue.put(['    - Emptying Sockets...\n\n', '', 0, 2])

        # Reset Counter
        count = 0

        # Start Socket Emptying Loop
        while count < 1000:

            # Select available socket to read
            read, write, err = select.select(self.gui.sockList, [], [],0)

            # If data is available, read the socket and discard it
            for sock in read:
                data, addr = sock.recvfrom(1024)

            # Increase the socket counter
            count += 1

            # Add a small wait time
            time.sleep(0.0015)

        # If the trigger with fast option is enabled, alert teh user and wait
        if self.gui.varEnableTrigger.get() == 1:
            self.queue.put(['    - Waiting for %s to run DAQ...\n\n' % self.gui.triggerSocket, '', 0, 2])
            self.triggerWait = True
            while self.triggerWait:
                # Loop though the available sockets until the Fast socket is ready

                if self.gui.varStopDAQ:
                    self.running = False
                    self.gui.varRunning = False
                    self.triggerWait = False
                    self.queue.put(
                            ['    - DAQ Stopped by User : %s\n\n' % time.asctime(time.localtime(time.time())), '', 0, 1])
                    break

                read, write, err = select.select(self.gui.sockList, [], [])
                for sock in read:
                    _, _ = sock.recvfrom(1024)
                    if sock.sockName == self.gui.triggerSocket:
                        self.triggerWait = False
                        break

        else:
            self.triggerWait = False

            # LOG
            self.gui.logger.info(' Running DAQ gathering thread...')

        # Initialize time
        initTime = time.clock()

        try:
            # Loop daq
            while self.running:

                # Pause the writing procedure
                while self.gui.varPaused:
                    time.sleep(0.1)
          
                # Select the sockets that are ready to be read
                read, write, err = select.select(self.gui.sockList, [], [], 0)

                # Calculate current time
                curTime = time.clock() - initTime

                # For every ready socket, read the data and send it to the queue
                for sock in read:

                    # Receive the data from the socktet
                    data, addr = sock.recvfrom(4096)

                    # Unpack data and convert to list
                    data = list(struct.unpack(str(sock.decodeStr), data))

                    # Make the comunication vector to the printing thread
                    daqValues = [curTime, sock.getsockname(), data]

                    # Send the data to the printing queue
                    self.writeQueue.put(daqValues)

                # Make a small break (AVOIDS IMPRECISIONS IN THE TIMESTAMP), Causes Problems in Other PC's
                #time.sleep(0.0015)

                # If the stop button is pressed
                if self.gui.varStopDAQ:
                    
                    # Send the message to the writer that the daq has ended
                    self.writeQueue.put('daqend')

                    self.running = False

                    # LOG
                    self.gui.logger.info(' Daqend Message succesfully sent')

                    # Break from the loop
                    break

            # LOG
            self.gui.logger.info(' DAQ Gathering thread succesfully executed!')

        except socket.timeout:
            self.queue.put(['    - [ERROR!] Socket Timeout\n      Please check Connections and Initialize again\n', '', 0, 1])
            self.writeQueue.put('daqendtimeout')
            self.gui.varInitialized = False
            self.gui.connStates = [0 for _ in len(self.gui.sockList)]  # Connection States of the Sockets
            # LOG
            self.gui.logger.error(' Socket Timeout. Please check Connections and Initialize again')

    # _____CLOSE PROGRAM METHOD: handle closing the program through X button____
    def close_program(self):

        # Prompt the user if they want to close the program
        if tkMB.askokcancel('Quit', 'Do you want to quit the program?'):
            # LOG
            self.gui.logger.info(' Program Terminated.\n')

            # destroy the tk instance and close
            self.master.destroy()
            sys.exit()

# Main Function run loop
if __name__ == "__main__":

    # Create the tk object
    root = tk.Tk()

    # Setup
    root.wm_title("FSE/GW DAQ")
    root.iconbitmap('resources\images\mainIcon.ico')

    # Start the threaded client and run the main loop
    ThreadedClient(root)
    root.mainloop()

